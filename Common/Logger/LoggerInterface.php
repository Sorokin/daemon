<?php
namespace Daemon\Common\Logger;

/**
 * Интерфейс логгера
 * @author Sorokin Ilya<sorokinIlya87@gmail.com>
 */
interface LoggerInterface
{
    public function debug(string $message, ...$params);
    public function info(string $message, ...$params);
    public function warning(string $message, ...$params);
    public function error(string $message, ...$params);
    public function fatal(string $message, ...$params);
}