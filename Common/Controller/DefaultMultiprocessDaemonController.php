<?php
namespace Daemon\Common\Controller;

use Daemon\Common\Config\MultiprocessDaemonConfigInterface;
use Daemon\Common\Config\DaemonConfigInterface;
use Daemon\Common\Logger\LoggerInterface;

/**
 * Абстрактный класс многопоточного демона
 * @author Sorokin Ilya<sorokinIlya87@gmail.com>
 */
abstract class DefaultMultiprocessDaemonController extends DefaultDaemonController
{
    private $childProcesses = array();
    private $processType;

    const PARENT_TYPE = 'parent';
    const CHILD_TYPE = 'child';
    const SLEEP_TIME = 100000;

    /**
     * Инициализация демона
     * @param MultiprocessDaemonConfigInterface $config
     * @param LoggerInterface $logger
     */
    public function __construct(DaemonConfigInterface $config, LoggerInterface $logger)
    {
        if (!($config instanceof MultiprocessDaemonConfigInterface)){
            throw new \Exception('Config must be instance of Daemon\Config\MultiprocessDaemonConfigInterface');
        }
        parent::__construct($config, $logger);
    }

    /**
     * Обработчик сигнала SIGTERM
     */
    public function terminatedSignalHandler()
    {
        parent::terminatedSignalHandler();
        $this->sendSignalToChilds(SIGTERM);
    }

    public function usr2SignalHandler()
    {
        parent::usr2SignalHandler();
        // Если родитель, то посылаем всем дочкам сигнал на завершение, в противном случае игнорируем
        if(empty($this->processType) || $this->processType == self::PARENT_STATUS){
            $this->sendSignalToChilds(SIGTERM);
        }
    }

    /**
     * Устанавливает обработчик сигналов
     * @param type $signal
     * @param type $handler
     * @param type $processType
     * @return boolean
     */
    protected function setSignalHandler($signal, $handler)
    {
        if ($this->processType != self::CHILD_TYPE){
            // Не меняем изменяем обработчик сигналов для процессов другого типа
            return false;
        }

        if ($signal == SIGTERM){
            // Переопределить SIGTERM нельзя
            return false;
        }
        return pcntl_signal($signal, $handler);
    }

    /**
     * Отправляет сигнал всем дочерним процессам
     * @param int $signal
     */
    private function sendSignalToChilds(int $signal)
    {
        foreach($this->childProcesses as $pid => $timeStamp){
            posix_kill($pid, $signal);
            $this->logger->info('Send signal "'.$signal.'" for pid "'.$pid.'"');
        }
    }

    /**
     * Обработчик сигналов от дочерних процессов
     * @param int $pid
     * @param int $status
     * @return null
     */
    private function processChildSignal(int $pid, int $status)
    {
        if ($status !== 0){
            $this->logger->error('Recived bad terminate signal from child',array('pid' => $pid, 'status' => $status));
        }
     
        if ($pid > 0){
            unset($this->childProcesses[$pid]);
            return;
        }
        
        if ($pid === -1){
            $this->childProcesses = array();
            return;
        }
    }

    /**
     * Выполнение итерации.
     */
    protected function work()
    {
        if (empty($this->processType) || $this->processType == self::PARENT_TYPE){
            $this->parentWork();
        } else {
            $this->childWork();
        }
    }

    /**
     * Метод выполниться перед запуском бесконечного цикла итераций.
     * Использовать для инициализации соединений, открытия дескрипторов и т.д.
     */
    protected function preprocess()
    {

    }

    /**
     * Метод выполниться после получения команды завершения работы демона.
     * Использовать для закрытия соединений, дескрипторов и т.д.
     */
    protected function postprocess()
    {
        if ($this->processType == self::CHILD_TYPE){
            $this->childPostprocess();
        }
    }

    /**
     * Создаёт дочерние процессы а так же "слушает" сигналы от них
     * @return null
     * @throws \Exception
     */
    private function parentWork()
    {
        $status = null;
        if ($this->config->getCountProcess() <= count($this->childProcesses)){
            $pid = pcntl_waitpid(-1, $status, WNOHANG);
            if ($pid !== 0){
                $this->processChildSignal($pid, $status);
            }
            usleep(self::SLEEP_TIME);
            return;
        }

        $pid = pcntl_fork();
        if ($pid === -1){
            throw new \Exception('Could not run child process');
        } elseif ($pid === 0){
            $this->childPreprocess();
            $this->processType = self::CHILD_TYPE;
            return;
        }
        $this->processType = self::PARENT_TYPE;
        $this->childProcesses[$pid] = time();
    }

    /**
     * Выполнение итерации родительского процесса.
     * Выполненяется в бесконечном цикле.
     */
    abstract protected function childWork();

    /**
     * Метод выполниться перед запуском бесконечного цикла итераций дочернего процесса.
     * Использовать для инициализации соединений, открытия дескрипторов и т.д.
     */
    abstract protected function childPreprocess();

    /**
     * Метод выполниться после получения команды завершения работы демона в дочернем процессе.
     * Использовать для закрытия соединений, дескрипторов и т.д.
     */
    abstract protected function childPostprocess();
}