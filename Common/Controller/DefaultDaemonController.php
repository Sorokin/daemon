<?php
namespace Daemon\Common\Controller;

use Daemon\Common\Logger\LoggerInterface;
use Daemon\Common\Exception\ConnectionException;
use Daemon\Common\Config\DaemonConfigInterface;
use Daemon\Common\Exception\ApplicationException;

/**
 * Абстрактный класс однопоточного демона
 * @author Sorokin Ilya<sorokinIlya87@gmail.com>
 */
abstract class DefaultDaemonController
{
    const RUN_STATUS = 1;
    const STOP_STATUS = 2;

    protected $status;
    protected $config;
    protected $logger;

    /**
     * Инициализация демона
     * @param DaemonConfigInterface $config
     * @param LoggerInterface $logger
     */
    public function __construct(DaemonConfigInterface $config, LoggerInterface $logger)
    {
        $this->config = $config;
        $this->logger = $logger;
        // Функция выхода с фаталом
        register_shutdown_function(array($this, 'shutbownHandler'));
        set_error_handler(array($this,'errorHandler'), E_ALL);
        
        //CLOSE STD STREAM
        fclose(STDIN);
        $i = fopen('/dev/null', 'r');
        fclose(STDOUT);
        $o = fopen('/dev/null', 'w');
        fclose(STDERR);
        $e  = fopen('/dev/null', 'w');
    }

    public static function errorHandler($level, $message, $file, $line)
    {
        if ($level == E_STRICT){
            return;
        }
        throw new ApplicationException($message, $level, $file, $line);
    }

    public function shutbownHandler()
    {
        $error = error_get_last();
        if (empty($error) || $error['type'] == E_STRICT){
            return;
        }
        $this->logger->fatal($error['message'], array(
            'file' => $error['file'],
            'line' => $error['line'],
            'type' => $error['type']
        ));
    }

    /**
     * Обработчик сигнала SIGTERM
     */
    public function terminatedSignalHandler()
    {
        $this->logger->info('Recived terminate signal for pid "'.getmypid().'"', array(
            'daemon' => get_class($this)
        ));
        $this->status = self::STOP_STATUS;
    }

    public function usr2SignalHandler()
    {
        // Закрываем соединения
        $this->postprocess();
        // Открываем соединения заново
        $this->preprocess();
    }

    /**
     * Остановка демона
     * @return boolean
     */
    public function stop()
    {
        if (!$this->isActiveDaemon()){
            $this->logger->info('Daemon "'.get_class($this).'" not running');
            return false;
        }
        $pid = trim(file_get_contents($this->config->getPidFile()));
        $this->logger->info('Send terminate signal for pid "'.$pid.'"');
        posix_kill($pid, SIGTERM);
        for($i = 0; $i <= 15; $i++){
            if (!posix_kill($pid, 0)){
                return true;
            }
            usleep(200000);
        }
        $this->logger->info('Send kill signal for pid "'.$pid.'"');
        posix_kill($pid, SIGKILL);
        return false;
    }

    /**
     * Перезапуск демона
     */
    public function restart()
    {
        $this->stop();
        $this->start();
    }

    /**
     * Запуск демона
     * @throws \Exception
     */
    public function start()
    {
        $this->checkEnviroment();
        if ($this->isActiveDaemon()){
            throw new \Exception('Daemon "'.get_class($this).'" already running');
        }
        // Отвязываем демона от консоли
        $pid = pcntl_fork();
        if ($pid === -1){
            throw new \Exception('Could not run child process');
        }
        if ($pid > 0){
            exit();
        }
        $status = posix_setsid();
        if ($status == -1){
            throw new \Exception('Could not detach from terminal');
        }
        pcntl_signal(SIGTERM, array($this, 'terminatedSignalHandler'));
        pcntl_signal(SIGUSR2, array($this, 'usr2SignalHandler'));
        pcntl_signal_dispatch();
        file_put_contents($this->config->getPidFile(),getmypid());

        $this->status = self::RUN_STATUS;

        try {
            $this->preprocess();
        } catch (\Exception $e) {
            $this->logger->error('Can\'t init. Error:'.$e->getMessage().' in file "'.$e->getFile().' on line'.$e->getLine());
            return;
        }

        $countConnectionException = 0;
        while($this->status == self::RUN_STATUS ){
            try {
                $this->work();
                $countConnectionException = 0;
            } catch (ConnectionException $e) {
                $countConnectionException++;
                $this->processConnectionException($e, $countConnectionException);
            } catch (\Exception $e) {
                $this->logger->error($e->getMessage().' in file "'.$e->getFile().' on line'.$e->getLine());
            }
            pcntl_signal_dispatch();
        }
        $this->postprocess();
    }

    /**
     * Пауза
     */
    protected function sleep()
    {
        usleep($this->config->getSleepTime());
    }

    /**
     * Устанавливает обработчик сигналов
     * @param type $signal
     * @param type $handler
     * @return boolean
     */
    protected function setSignalHandler($signal, $handler)
    {
        if ($signal == SIGTERM){
            // Переопределить SIGTERM нельзя
            return false;
        }
        return pcntl_signal($signal, $handler);
    }

    /**
     * Проверка окружения
     * @throws \Exception
     */
    private function checkEnviroment()
    {
        $filename = $pidFileName = $this->config->getPidFile();

        if (!file_exists($pidFileName)){
            $filename = dirname($pidFileName);
        }

        if (!is_writable($filename) || !is_readable($filename)){
            throw new \Exception('Pid file "'.$pidFileName.'" must be readable and writeable');
        }
    }

    /**
     * Запущен ли демон
     * @return bool
     */
    private function isActiveDaemon(): bool
    {
        if (!file_exists($this->config->getPidFile())){
            return false;
        }

        $pid = trim(file_get_contents($this->config->getPidFile()));
        $status = posix_kill($pid, 0);

        if ($status){
            return true;
        }
        unlink($this->config->getPidFile());
        return false;
    }

    /**
     * Обработчик исключений ошибок сети
     * @param ConnectionException $e
     */
    protected function processConnectionException(ConnectionException $e, $num)
    {
        if ($num > 3){
            $this->logger->fatal($e->getMessage().' in file "'.$e->getFile().' on line'.$e->getLine());
            $this->terminatedSignalHandler();
            return;
        }
        $sleepTime = $num * 60;
        $this->logger->error($e->getMessage().' in file "'.$e->getFile().' on line'.$e->getLine());

        // Закрываем соединения
        try {
            $this->postprocess();
        } catch (\Throwable $e) {
            // Ничего не делаем
        }
        $this->logger->info('Sleep '.$sleepTime.' seconds');
        sleep($sleepTime);
        // Пытаемся открыть соединения заново
        try {
            $this->preprocess();
        } catch (ConnectionException $e) {
            $this->processConnectionException($e, ++$num);
        }
        return;
    }

    /**
     * Выполнение итерации. Выполненяется в бесконечном цикле.
     */
    abstract protected function work();

    /**
     * Метод выполниться перед запуском бесконечного цикла итераций.
     * Использовать для инициализации соединений, открытия дескрипторов и т.д.
     */
    abstract protected function preprocess();

    /**
     * Метод выполниться после получения команды завершения работы демона.
     * Использовать для закрытия соединений, дескрипторов и т.д.
     */
    abstract protected function postprocess();
}