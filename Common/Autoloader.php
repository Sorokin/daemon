<?php
namespace Daemon\Common;

define('DAEMON_LIB_BASE_DIR', __DIR__.'/../../');

spl_autoload_register(
    function(string $className)
    {
        $fileName = str_replace(
            array( 'Daemon\\', '\\' ),
            array( 'daemon/', '/'),
            $className
        ).'.php';

        if (file_exists(DAEMON_LIB_BASE_DIR.'/'.$fileName)){
            include DAEMON_LIB_BASE_DIR.'/'.$fileName;
        }
    }
);