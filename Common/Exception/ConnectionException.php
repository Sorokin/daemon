<?php
namespace Daemon\Common\Exception;

/**
 * Класс исключения при сбоях в сети.
 * @author Sorokin Ilya<sorokinIlya87@gmail.com>
 */
class ConnectionException extends \Exception
{}