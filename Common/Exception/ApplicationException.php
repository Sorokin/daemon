<?php
namespace Daemon\Common\Exception;

/**
 * Description of ApplicationException
 *
 * @author sorokin
 */
class ApplicationException extends \Exception
{
    public function __construct(string $message = '', int $code = 0, string $file = '', int $line = null)
    {
        parent::__construct($message, $code);
        $this->file = $file;
        $this->line = $line;
    }
}