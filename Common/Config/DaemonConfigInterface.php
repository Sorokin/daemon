<?php
namespace Daemon\Common\Config;

/**
 * Интерфейс конфига однопоточного демона
 * @author Sorokin Ilya<sorokinIlya87@gmail.com>
 */
interface DaemonConfigInterface
{
    /**
     * Возвращает паузу после каждой итерации(в микросекундах)
     */
    public function getSleepTime():int;

    /**
     * Возвращает путь к pid фалу
     */
    public function getPidFile():string;

    /**
     * Возвращает название класса демона
     */
    public function getClassName():string;

    /**
     * Возвращает параметры инициализации
     * На данный момент не используется
     */
    public function getParams();
}