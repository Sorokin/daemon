<?php
namespace Daemon\Common\Config;

/**
 * Интерфейс конфига многопоточного демона
 * @author Sorokin Ilya<sorokinIlya87@gmail.com>
 */
interface MultiprocessDaemonConfigInterface extends DaemonConfigInterface
{
    /**
     * Возвращает количество дочерних процессов
     */
    public function getCountProcess(): int;
}