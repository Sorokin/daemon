<?php
namespace Daemon\Example\Controller;

use Daemon\Common\Controller\DefaultDaemonController;

class ExampleDaemon extends DefaultDaemonController
{
    protected function postprocess()
    {
        $this->logger->info('postprocess');
        // do nothing
    }

    protected function preprocess()
    {
        $this->logger->info('preprocess');
        // do nothing
    }

    protected function work()
    {
        $this->logger->info('I\'m work');
        $this->sleep();
    }
}