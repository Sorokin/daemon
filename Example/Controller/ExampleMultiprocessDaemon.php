<?php
namespace Daemon\Example\Controller;

use Daemon\Common\Controller\DefaultMultiprocessDaemonController;

class ExampleMultiprocessDaemon extends DefaultMultiprocessDaemonController
{
    protected function childPostprocess()
    {
        // do nothing
    }

    protected function childPreprocess()
    {
        // do nothing
    }

    protected function childWork()
    {
        $this->logger->info('I\'m work. Child:'.getmypid());
    }
}