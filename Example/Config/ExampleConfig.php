<?php
namespace Daemon\Example\Config;

use Daemon\Common\Config\DaemonConfigInterface;

class ExampleConfig implements DaemonConfigInterface
{
    public function getClassName(): string
    {
        return '\\Daemon\\Example\\Controller\\ExampleDaemon';
    }

    public function getParams()
    {
        return array();
    }

    public function getPidFile(): string
    {
        return BASE_DIR.'/pids/example-daemon.pid';
    }

    public function getSleepTime(): int
    {
        return 1000000;
    }
}