<?php
namespace Daemon\Example\Config;

use Daemon\Common\Config\MultiprocessDaemonConfigInterface;

class ExampleMultiprocessConfig extends ExampleConfig implements MultiprocessDaemonConfigInterface
{
    public function getClassName(): string
    {
        return 'Daemon\\Example\\Controller\\ExampleMultiprocessDaemon';
    }

    public function getPidFile(): string
    {
        return BASE_DIR.'/pids/example-multiprocess-daemon.pid';
    }

    public function getCountProcess(): int
    {
        return 2;
    }
}