<?php
namespace Daemon;

include dirname(__FILE__).'/autoloader.php';

use \Daemon\Example\Config\ExampleMultiprocessConfig;
use \Daemon\Example\Logger\ExampleLogger;

function printInfo()
{
    echo '************************************************'."\n";
    echo '* php runMultiprocess.php (start|restart|stop) *'."\n";
    echo '************************************************'."\n";
}

try {
    $config = new ExampleMultiprocessConfig();
    $logger = new ExampleLogger(BASE_DIR.'/logs/multiprocess-main.log');
    $daemon = $config->getClassName();
    $controller = new $daemon($config, $logger);
    
    if (!isset($argv[1])){
        printInfo();
        exit;
    }

    switch ($argv[1]){
        case 'run':
        case 'start':
            $controller->start();
        break;
        case 'restart':
        case 'reload':
            $controller->restart();
        break;
        case 'stop':
            $controller->stop();
        break;
        default:
            printInfo();
            exit;
    }
} catch(\Throwable $e) {
    echo $e->getMessage()
    //.' in file '.$e->getFile().' on line '.$e->getLine()
    ."\n";
}