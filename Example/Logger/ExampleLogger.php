<?php
namespace Daemon\Example\Logger;

use Daemon\Common\Logger\LoggerInterface;

class ExampleLogger implements LoggerInterface
{
    private $resource;

    public function __construct(string $fileName)
    {
        if (file_exists($fileName)){
            if (!is_writeable($fileName)){
                throw new \Exception('Can\'t write in file "'.$fileName.'"');
            }
        } else {
            $path = dirname($fileName);
            if (!is_writable($path)){
                throw new \Exception('Can\'t write in path "'.$path.'"');
            }
        }
        $this->resource = fopen($fileName, 'a');
    }
    
    public function debug(string $message, ...$params)
    {
        $this->write('DEBUG', $message, $params);
    }

    public function error(string $message, ...$params)
    {
        $this->write('ERROR', $message, $params);
    }

    public function fatal(string $message, ...$params)
    {
        $this->write('FATAL', $message, $params);
    }

    public function info(string $message, ...$params)
    {
        $this->write('INFO', $message, $params);
    }

    public function warning(string $message, ...$params)
    {
        $this->write('WARNING', $message, $params);
    }

    private function write(string $level, string $message, array $params = array())
    {
        fwrite($this->resource, $this->getRow($message, $level, $params));
    }

    private function getRow(string $message, string $level, array $params = array()): string
    {
        return
            date('Y-m-d H:i:s').' ['.$level.'] '.$message.
            (!empty($params) ? PHP_EOL.var_export($params,true) : '').
            PHP_EOL;
    }

    public function __destruct()
    {
        fclose($this->resource);
    }
}