<?php
define('BASE_DIR', dirname(dirname(__FILE__)));
function autoload_psr4($class)
{
    return (
        @include
            BASE_DIR.'/'.
            str_replace(
                '\\',
                '/',
                preg_replace('/^Daemon\\\/i', '', $class)
            ).
            '.php'
    );
}
spl_autoload_register('autoload_psr4');